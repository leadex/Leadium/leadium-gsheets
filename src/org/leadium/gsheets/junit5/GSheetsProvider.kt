package org.leadium.gsheets.junit5

import org.apache.commons.lang3.tuple.Pair
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.leadium.core.utils.replaceCamelCaseWithSpaces
import org.leadium.gsheets.AbstractGSheets
import java.io.IOException
import java.security.GeneralSecurityException
import java.util.logging.Logger
import java.util.stream.Stream

class GSheetsProvider : ArgumentsProvider, AbstractGSheets() {

    private val log = Logger.getLogger(this.javaClass.simpleName)

    @Target(AnnotationTarget.FUNCTION)
    annotation class ReferenceAsDisplayName

    @Target(AnnotationTarget.FUNCTION)
    annotation class Settings(
        val spreadsheetId: String,
        val envSystemPropertyKey: String,
        val envPlaceholder: String,
        val range: String,
        val testNameColumn: String,
        val numLinesToSkip: Int = 0
    )

    /**
     * Prints the names and majors of students in a sample spreadsheet:
     * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
     */
    @Throws(IOException::class, GeneralSecurityException::class)
    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> {
        val referenceAsDisplayName = context.testMethod.get().isAnnotationPresent(ReferenceAsDisplayName::class.java)
        val settings = context.testMethod.get().getAnnotation(Settings::class.java)
        val envSystemPropertyKey = System.getProperty(settings.envSystemPropertyKey)
        val range = settings.range.replace(settings.envPlaceholder, envSystemPropertyKey)
        val response = buildService().spreadsheets().values()[settings.spreadsheetId, range].execute()
        val data = response.getValues()
        val arguments = ArrayList<Arguments>()
        if (data == null || data.isEmpty()) {
            log.warning("No data found.")
        } else {
            data.forEachIndexed { rowIndex, row ->
                val rowData = LinkedHashMap<Any, Pair<Any, Any>>()
                row.forEachIndexed { columnIndex, _ ->
                    rowData.addRow(data, row, columnIndex)
                }
                if (rowIndex >= settings.numLinesToSkip) {
                    var testName = rowData[settings.testNameColumn]!!.left as String
                    if (referenceAsDisplayName) testName = testName.toDisplayName()
                    val map = rowData as LinkedHashMap<*, *>
                    log.info(map.toString())
                    arguments.add(Arguments.of(rowIndex, testName, map))
                }
            }
        }
        return arguments.stream()
    }

    private fun String.toDisplayName(): String {
        val sb = StringBuilder()
        this.replaceCamelCaseWithSpaces()
            .replace("_", " ")
            .split(".")
            .reversed()
            .forEach { s: String -> sb.append(s).append(" | ") }
        return sb.toString().substring(0, sb.toString().length - 3)
    }

    private fun HashMap<Any, Pair<Any, Any>>.addRow(
        values: List<List<Any>>,
        row: List<Any>,
        columnIndex: Int
    ): HashMap<Any, Pair<Any, Any>> {
        val map = this
        map[values[0][columnIndex]] = Pair.of(row[columnIndex], Any())
        return map
    }
}