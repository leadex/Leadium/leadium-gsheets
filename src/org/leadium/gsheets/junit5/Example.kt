package org.leadium.gsheets.junit5

import org.apache.commons.lang3.tuple.Pair
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ArgumentsSource
import org.leadium.core.TestCase

@DisplayName("Example API Tests")
class Example {

    companion object {
        const val spreadsheetId = "1ezi0MqvZhYPs0IiX0_jIBkotJPAs3eEa2uFVT-z2Q5Q"
        const val envSystemPropertyKey = "envTestDataDir"
        const val envPlaceholder = "env"
        const val sheetName = "debug-$envPlaceholder"
        const val testCaseReferenceKey = "testCaseReference"
        const val testNameColumn = "testCaseReference"
        const val testDescriptionColumn = "testDescription"
    }

    @ParameterizedTest(name = "{1}")
    @ArgumentsSource(GSheetsProvider::class)
    @GSheetsProvider.ReferenceAsDisplayName
    @GSheetsProvider.Settings(
        spreadsheetId = spreadsheetId,
        envSystemPropertyKey = envSystemPropertyKey,
        envPlaceholder = envPlaceholder,
        range = sheetName,
        testNameColumn = testNameColumn,
        numLinesToSkip = 1
    )
    fun test(rowIndex: Int, testName: String, testData: LinkedHashMap<Any, Pair<String, Any>>) {
        val testCaseClass = Class.forName(testData[testCaseReferenceKey]!!.left)
        val testCase = testCaseClass.getDeclaredConstructor(testData.javaClass).newInstance(testData) as TestCase
//        DescriptionAppender(System.getProperty("rootProjectPath"), System.getProperty("gitHubMasterUrl"))
//            .appendTestCaseDescription(testData[testDescriptionColumn]!!.left)
//            .appendLinkToTestCase(testCase)
//            .appendLinkToGSheet(spreadsheetId)
//            .appendTestDataMap(testData)
//            .apply()
        testCase.begin()
    }
}