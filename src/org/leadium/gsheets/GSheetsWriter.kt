package org.leadium.gsheets

import com.google.api.services.sheets.v4.model.UpdateValuesResponse
import com.google.api.services.sheets.v4.model.ValueRange
import org.leadium.gsheets.AbstractGSheets

class GSheetsWriter(private val spreadsheetId: String) : AbstractGSheets() {

    fun writeTestKey(sheet: String, rowIndex: Int, xrayTestKey: String) {
        val values: List<List<Any>> = listOf(listOf(xrayTestKey))
        write("${sheet}A${rowIndex + 1}", values)
    }

    private fun write(range: String, values: List<List<Any>>) {
        val body: ValueRange = ValueRange()
            .setValues(values)
        val result: UpdateValuesResponse = buildService().spreadsheets().values().update(spreadsheetId, range, body)
            .setValueInputOption("RAW")
            .execute()
        System.out.printf("%d cells updated.", result.updatedCells)
    }
}